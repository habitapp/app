import { Component } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'page-offline',
  templateUrl: 'offline.html'
})
export class OfflinePage {
  relevamientos_offline : object[] = [];

  constructor(private storage: NativeStorage,
    private sanitizer: DomSanitizer) {

    this.obtener_relevamientos();
  }

  obtener_relevamientos () : void {
    this.storage.keys().then(keys => {
      keys.forEach(key => {
        if (!key.startsWith('relevamiento_offline_')) return;

        this.storage.getItem(key).then(relevamiento => {
          this.relevamientos_offline.push(relevamiento['data']);
        });
      });
    });
  }

  getSanitizedPhoto(relevamiento) : SafeResourceUrl {
    const photo = relevamiento['attributes']['fotos']['principal'];

    return this.sanitizer.bypassSecurityTrustResourceUrl(photo);
  }
}
