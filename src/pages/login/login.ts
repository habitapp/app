import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private login_form: FormGroup;
  private password: string = '';
  private identifier: string = '';

  constructor (private viewCtrl: ViewController) {
    this.login_form = new FormGroup({
      identifier: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  sendForm () {
    this.viewCtrl.dismiss(this.login_form.value);
  }

  validForm () : boolean {
    return this.login_form.valid;
  }
}
