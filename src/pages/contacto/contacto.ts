import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SubscripcionesProvider } from '../../providers/subscripciones/subscripciones';
import { ApiProvider } from '../../providers/api/api';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { InstanciaProvider } from '../../providers/instancia/instancia';

@Component({
  selector: 'page-contacto',
  templateUrl: 'contacto.html',
})
export class ContactoPage {
  private contacto_form: FormGroup;
  private email: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder,
    private storage: NativeStorage,
    private api: ApiProvider,
    private instancia: InstanciaProvider,
    private subscripciones: SubscripcionesProvider) {

    this.storage.getItem('email').then(
      data => this.email = data,
      error => null);

    this.contacto_form = this.formBuilder.group({
      email: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    // Autenticarse solo ahora, asi no creamos una cuenta por cada
    // visita individual
    this.instancia.authenticate();
  }

  yaSeSuscribio() {
    return (this.email) ? true : false;
  }

  sendForm() {
    var data = this.prepareData();
    this.subscripciones.create(data)
      .subscribe(res => {
        var email = this.contacto_form.value.email;
        if (res.status == 201) {
          this.storage.setItem('email', email).then(() => {
            this.email = email;
            this.contacto_form.reset();
          });
        }
      }, error => console.log(error));
  }

  prepareData() : Object {
    return {
      data: {
        type: 'subscripcion',
        attributes: {
          email: this.contacto_form.value.email
        }
      }
    };
  }

}
