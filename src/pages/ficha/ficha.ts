import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RelevamientosProvider } from '../../providers/relevamientos/relevamientos';
import { ApiProvider } from '../../providers/api/api';
import { ComentariosProvider } from '../../providers/comentarios/comentarios';
import * as moment from 'moment';
import 'moment/locale/es';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RelevamientoPage } from '../relevamiento/relevamiento';

@Component({
  selector: 'page-ficha',
  templateUrl: 'ficha.html',
})
export class FichaPage {
  private relevamiento_id: number;
  private relevamiento: Object;
  private error: string;
  private comentarios: Object[];
  private comentario_form: FormGroup;
  private login: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private relevamientos: RelevamientosProvider,
    private api: ApiProvider,
    private formBuilder: FormBuilder) {

    moment.locale('es');

    this.relevamiento_id = this.navParams.get('relevamiento_id');

    this.login = this.navParams.get('login');
    // Compatibilidad con la app
    if (!this.api.browser) this.login = true;

    this.relevamientos.find(this.relevamiento_id).subscribe(res => {
      if (res.status === 200) {
        this.relevamiento = res.body;
      } else{
        this.error = 'No se pudo encontrar el relevamiento solicitado';
      }
    });

    this.relevamientos.comentarios.getAll(this.relevamiento_id).subscribe(res => {
      if (res.status == 200) {
        this.comentarios = res.body['data'];
      }
    });

    this.comentario_form = this.formBuilder.group({
      contenido: ['', Validators.required]
    });
  }

  humanUnixAgo(date) {
    return moment(date, 'X').fromNow();
  }

  ionViewDidLoad() {
  }

  getPhoto(name : string) : string {
    return this.relevamiento['data']['attributes']['thumbs'][name];
  }

  sendForm() {
    var data = this.prepareData();
    this.relevamientos.comentarios.create(this.relevamiento_id, data)
      .subscribe(res => {
        if (res.status == 201) {
          this.comentarios.push(res.body['data']);
          this.comentario_form.reset();
        }
      }, error => console.log(error));
  }

  prepareData() : Object {
    return {
      data: {
        type: 'comentario',
        attributes: {
          contenido: this.comentario_form.value.contenido
        }
      }
    };
  }

  actualizarRelevamiento(relevamiento : object) {
    this.navCtrl.push(RelevamientoPage, {
      relevamiento: relevamiento,
      coordenadas: relevamiento['data']['attributes']['coordenadas']
    });
  }
}
