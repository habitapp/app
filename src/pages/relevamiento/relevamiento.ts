import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { RelevamientosProvider } from '../../providers/relevamientos/relevamientos';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { NominatimProvider, Nominatim } from '../../providers/nominatim/nominatim';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { ApiProvider } from '../../providers/api/api';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  selector: 'page-relevamiento',
  templateUrl: 'relevamiento.html',
})
export class RelevamientoPage {
  private relevamiento_form: FormGroup;
  private coordenadas: Object;
  private todas_las_categorias: { data: any[] } = { data: [] };
  private categorias_raiz: Object;
  private fotos: object = { principal: '' };
  private address: string;
  private relevamiento_original: object = { data: { attributes: { fotos: {}, thumbs: {} } }, included: [] };
  private opciones_de_camara: CameraOptions = {
    targetWidth: 1280,
    targetHeight: 1280,
    correctOrientation: true,
    saveToPhotoAlbum: true,
    cameraDirection: 1
  };
  private extra_controls : Array<object> = [];

  constructor(public navCtrl: NavController,
    private platform: Platform,
    public navParams: NavParams,
    private relevamientos: RelevamientosProvider,
    private categorias: CategoriasProvider,
    private nominatim: NominatimProvider,
    private toast: ToastController,
    private file: File,
    private api: ApiProvider,
    private sanitizer: DomSanitizer,
    private storage: NativeStorage,
    private camara: Camera) {

    if (this.navParams.get('relevamiento')) {
      this.relevamiento_original = this.navParams.get('relevamiento');
    }

    var r = this.relevamiento_original['data']['attributes'];
    this.relevamiento_form = new FormGroup({
      direccion: new FormControl((r['direccion'] || ''), Validators.required),
      descripcion: new FormControl((r['descripcion'] || '')),
      informacion_de_contacto: new FormControl((r['informacion_de_contacto'] || '')),
      categoria_id: new FormControl((r['categoria_id'] || ''), Validators.required),
      sub_categoria_id: new FormControl((r['sub_categoria_id'] || ''), Validators.required),
      extra: new FormGroup({})
    });

    // La primera foto del array es la principal
    if (r['fotos']) {
      this.fotos['principal'] = r['fotos']['principal'];
    }

    this.coordenadas = this.navParams.get('coordenadas');

    if (this.relevamiento_original['included'].length > 0) this.extraForm();

    if (!this.relevamiento_original['data']['id']) {
      this.nominatim.get(this.coordenadas['lat'], this.coordenadas['lng'])
        .subscribe(
          data => this.address = data.display_name,
          error => console.log(error));
    }
  }

  categoriasRaiz() {
    // Obtener todas las categorías raíz
    this.categorias_raiz = this.todas_las_categorias.data
      .filter(categoria => {
        if (categoria.attributes.raiz_id == null) return categoria;
      });
  }

  subCategorias(categoria_raiz : number) : Object[] {
    return this.todas_las_categorias.data.filter(categoria => {
      if (categoria.attributes.raiz_id == categoria_raiz) {
        return categoria;
      }
    });
  }

  ionViewDidLoad() {
    this.categorias.getAll(true).subscribe((data: { data: any[] }) => {
      this.todas_las_categorias.data = data.data;
      this.categoriasRaiz();
    }, error => {
      this.storage.getItem('cache_categorias').then(data => {
        this.todas_las_categorias.data = data.data;
        this.categoriasRaiz();
      });
    });
  }

  showToast(message: string) {
    const toast = this.toast.create({
      duration: 3000,
      message: message
    });

    toast.present();
  }

  getPicture(source: number, name: string = 'principal') {
    var opciones = this.opciones_de_camara;
    opciones.sourceType = source;

    if (name == 'principal') {
      opciones.targetWidth = 1280;
      opciones.targetHeight = 1280;
    } else {
      opciones.targetWidth = 600;
      opciones.targetHeight = 600;
    }

    // Esperar hasta que aparezca el selector de archivos y abrirlo
    // automáticamente
    if (this.api.browser && source == 0) {
      setTimeout(() => {
        var camera : HTMLElement = document.getElementsByClassName('cordova-camera-select')[0] as HTMLElement;
        camera.click()
      }, 100);
    }

    this.camara.getPicture(opciones)
      .then(
        image => {
          if (image.startsWith('file://')) {
            this.fotos[name] = image;
          } else {
            this.fotos[name] = 'data:image/jpeg;base64, '+image;
          }
        },
        error => this.showToast('Error tomando foto :('));
  }

  getSanitizedPhoto(name: string = 'principal') : SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.getPhoto(name));
  }

  getPhoto(name: string = 'principal') : string {
    return (this.fotos[name]) ? this.fotos[name] : '';
  }

  validForm() : boolean {
    return this.relevamiento_form.valid;
  }

  sendForm() {
    var data = this.prepareData();
    var base64 = [];
    Object.keys(this.fotos).forEach(name => {
      var photo = this.getPhoto(name);
      if (!photo) return;

      if (photo.startsWith('file://')) {
        var file_parts = photo.split('/');
        var file = file_parts.pop().split('?').shift();

        // Agregamos todos los base64 al objeto de fotos y devolvemos
        // una promesa para poder resolverlas todas juntas al final
        base64.push(this.file.readAsDataURL(file_parts.join('/'), file).then(base64 => {
          data.data.attributes.fotos[name] = base64;
          return new Promise((res,rej) => res(true));
        }));
      } else if (photo.startsWith('data:')) {
        data.data.attributes.fotos[name] = photo;
      }
    });

    Promise.all(base64).then(values => {
      this.sendData(data);
    });
  }

  sendData(data : any) {
    this.showToast('Enviando...');
    // Enviar inmediatamente si estamos conectadxs
    if (this.api.conectado) {
      this.relevamientos.save(data).subscribe(res => {
        // No queremos decirle que no le quisimos crear el relevamiento
        if (res.status === 200 || res.status === 201) {
          this.volver(null, res.body);
        }
      }, error => {
        this.encolar(data,
          'Hubo un error al enviar, se guardará para después');
      });
    // Sino, guardar para después
    } else {
      this.encolar(data);
    }
  }

  encolar (data : any, mensaje : string = 'Se enviará mas tarde') {
    const date = Date.now().toString();
    const rand = Math.random().toString();

    this.storage.setItem(`relevamiento_offline_${date}_${rand}`, data)
      .then(
        () => this.volver(mensaje, data),
        error => this.showToast('¡Hubo un error! Intentá de vuelta')
      );
  }

  volver (gracias : string = '¡Gracias!', data : any) {
    this.showToast(gracias);
    // TODO parece que es un anti patrón
    // https://forum.ionicframework.com/t/solved-ionic-navcontroller-pop-with-params/58104/35
    var callback = this.navParams.get('callback');
    if (callback) { callback(data); }
    this.navCtrl.pop();
  }

  // Actualiza la subcategoria con el valor de la categoria raiz solo si
  // estamos cambiando de categoria.
  //
  // TODO hacer que la subcategoria solo aparezca cuando la categoria
  // raiz tiene subcategorias!
  defaultSubCategoria() {
    var cat = this.relevamiento_form.value.categoria_id;
    var sub_cat = this.subCategorias(cat);
    var included = sub_cat.filter(x => x['attributes']['id'] === this.relevamiento_form.value.sub_categoria_id);

    if (!included || cat !== this.relevamiento_form.value.sub_categoria_id) {
      this.relevamiento_form.controls.sub_categoria_id.patchValue(this.relevamiento_form.value.categoria_id);
      // TODO el patchValue() debería lanzar el evento, pero estamos
      // bindeando en ionChange en lugar de change.
      this.extraForm();
    }
  }

  possibleValue (control : string, value : string) {
    var values = this.relevamiento_form.value.extra[control]
      .split(',').filter(x => x !== '');

    if (values.includes(value)) {
      values = values.filter(x => x !== value);
    } else {
      values.push(value);
    }

    this.relevamiento_form.controls.extra['controls'][control].patchValue(values.join(','));
  }

  isPossibleChecked(control : string, value : string) {
    var values = this.relevamiento_form.value.extra[control]
      .split(',').filter(x => x !== '');

    return (values.includes(value));
  }

  // Traer la plantilla de la categoría y generar los campos
  // dinámicamente, usando como valores por defecto los del relevamiento
  // cargado.
  //
  // Si falla, buscamos los datos en la caché.  Si no hay caché no hay
  // mucho para hacer...
  extraForm() {
    this.relevamiento_form.removeControl('extra');
    this.extra_controls = [];

    const categoria = this.relevamiento_form.value.sub_categoria_id;

    this.categorias
      .find(categoria)
      .subscribe(
        data => this.generarControles(data),
        error => {
          this
            .storage
            .getItem(`categoria_${categoria}`)
            .then(data => this.generarControles(data));
        });
  }

  generarControles (data : object) {
    if (data['included'].length == 0) return;

    this.relevamiento_form.addControl('extra', new FormGroup({}));
    data['included'].forEach((item, i) => {
      // Solo procesamos plantillas
      if (item['type'] !== 'plantilla') return;

      // Define el valor por defecto, si es booleano, es falso
      var default_value = (item['attributes']['value'] == 'boolean') ? 'false' : '';
      // Encontrar el valor anterior
      var prev = this.relevamiento_original['included'].find(x => x['attributes']['plantilla_id'].toString() === item['id']);
      // Decidir si usamos el valor anterior o el por defecto
      var valor = (prev) ? prev['attributes']['valor'] : default_value;
      // Crear el control
      var control = (item['attributes']['required']) ? new FormControl(valor, Validators.required) : new FormControl(valor);
      // Nombre del campo
      var name = item['attributes']['name'];

      // No atorar la app con imágenes grandes, usar los thumbnails
      this.fotos[name] = this.relevamiento_original['data']['attributes']['thumbs'][name];

      // Agregar el control al formulario
      (<FormGroup>this.relevamiento_form.controls.extra).addControl(name, control);
    });

    this.extra_controls = data['included'];
  }

  isControlPopularDePrecios() : boolean {
    return (this.relevamiento_form.value.sub_categoria_id == this.api.control_popular_de_precios);
  }

  prepareData() {
    var data = {
      type: 'relevamientos',
      attributes: this.relevamiento_form.value
    };

    data['attributes']['categoria_id'] = data['attributes']['sub_categoria_id'];
    data['attributes']['fotos'] = {};
    data['attributes']['coordenadas'] = this.coordenadas;

    if (this.relevamiento_original['data']['id']) {
      data['id'] = this.relevamiento_original['data']['id'];
    }

    return { data: data };
  }
}
