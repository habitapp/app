import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { ApiProvider } from '../../providers/api/api';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html',
})
export class CategoriasPage {
  private todas_las_categorias: { data: any[] } = { data: [] };
  private categorias_guardadas: Object = {};
  private expandir: Object = {};
  private categorias_raiz: Object;
  private still_loading: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private categorias: CategoriasProvider,
    private storage: NativeStorage,
    private viewCtrl: ViewController,
    private api: ApiProvider,
    private toast: ToastController) {

    this.categorias_guardadas = this.viewCtrl.data.categorias_guardadas;
  }

  ionViewDidLoad() {
    // Si estamos desconectados, traer las categorías de la caché
    this.categorias.getAll().subscribe((data: { data: any[] }) => {
      this.todas_las_categorias.data = data.data;
      this.categoriasRaiz();
      this.still_loading = false;
    }, error => {
      this.storage.getItem('cache_categorias').then(data => {
        this.todas_las_categorias.data = data.data;
        this.categoriasRaiz();
        this.still_loading = false;
      });
    });
  }

  categoriasRaiz() {
    // Obtener todas las categorías raíz
    this.categorias_raiz = this.todas_las_categorias.data
      .filter(categoria => {
        if (categoria.attributes.raiz_id == null) return categoria;
      });
  }

  subCategorias(categoria_raiz : number) : Object[] {
    return this.todas_las_categorias.data.filter(categoria => {
      if (categoria.attributes.raiz_id == categoria_raiz) {
        return categoria;
      }
    });
  }

  showToast(message: string) {
    const toast = this.toast.create({
      message: message,
      closeButtonText: 'Ok',
      showCloseButton: true
    });

    toast.present();
  }

  toggleSubCategoria(categoria_raiz: number, force: boolean = false) {
    if (this.expandir[categoria_raiz]) {
      this.expandir[categoria_raiz] = !this.expandir[categoria_raiz];
    } else {
      this.expandir[categoria_raiz] = true;
    }
  }

  whichArrow(categoria_raiz: number) : string {
    return (this.expandir[categoria_raiz]) ? 'arrow-dropdown' : 'arrow-dropright';
  }

  // Queremos varios comportamientos:
  //
  // 1. Al seleccionar una categoría, seleccionar todas las
  //    subcategorías
  // 2. Al deseleccionar la categoría pero teniendo seleccionadas
  //    subcategorías, podemos ver las subcategorías sin la categoría
  // 3. Al deseleccionar la categoría teniendo todas las subcategorías
  //    seleccionadas sucede lo opuesto de 1
  seleccionarSubCategorias(categoria_raiz: number) {
    // Solo hacer este proceso si estamos expandiendo las subcategorías
    this.expandir[categoria_raiz] = true;
    var sub_categorias = this.subCategorias(categoria_raiz);

    var todas_falsas = sub_categorias.every(s => !this.categorias_guardadas[s['id']]);
    var todas_verdaderas = sub_categorias.every(s => this.categorias_guardadas[s['id']]);

    // Hay tres estados
    // Todas falsas => pintar
    // Todas verdaderas => despintar
    // Ni uno ni otro => nada
    if (!todas_falsas && !todas_verdaderas) {
    } else {
      sub_categorias.forEach(s => {
        this.categorias_guardadas[s['id']] = todas_falsas && !todas_verdaderas;
      });
    }

    this.save();
  }

  dismiss() {
    this.save();

    this.viewCtrl.dismiss(this.categorias_guardadas);
  }

  save() {
    this.storage
      .setItem('categorias_guardadas', this.categorias_guardadas)
      .then(() => true, error => false);
  }

}
