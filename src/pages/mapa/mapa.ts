import { Component } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { NavController, ActionSheetController, PopoverController, ToastController } from 'ionic-angular';
import L from 'leaflet';
import { RelevamientosProvider } from '../../providers/relevamientos/relevamientos';
import { InstanciaProvider } from '../../providers/instancia/instancia';
import { ApiProvider } from '../../providers/api/api';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { RelevamientoPage } from '../relevamiento/relevamiento';
import { CategoriasPage } from '../categorias/categorias';
import { LoginPage } from '../login/login';
import { FichaPage } from '../ficha/ficha';
import { NativeStorage } from '@ionic-native/native-storage';
import { Geolocation, GeolocationOptions } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';

@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html'
})
export class MapaPage {
  tmp_marker: L.Marker;
  map: L.Map;
  center: L.PointTuple;
  tiles: string = 'https://osm.habitapp.org/{z}/{x}/{y}.png';
  parcelario: string = 'https://habitapp.org/parcelario/{z}/{x}/{y}.png';;
  geojson: L.GeoJSON;
  zoom: number;
  sin_notificacion_en_zoom: boolean = false;
  categorias_guardadas: Object = {};
  opciones_de_geolocalizacion: GeolocationOptions = {
    enableHighAccuracy: true,
    timeout: 10000
  };
  login: boolean = false;

  // Crea un relevamiento en el mapa desde la carga de relevamiento
  crearRelevamientoAlVolver = (relevamiento: Object) => {
    const r = relevamiento['data']['attributes'];
    const c = r['coordenadas'];
    const color = (r['categoria']) ? r['categoria']['color'] : '#13487C';

    // Crear el marcador
    const m = new L.marker([c['lat'], c['lng']],
      { icon: this.getIcon(color) }).toGeoJSON();

    m.properties = { color: color };

    // Darle un ID al punto
    m.id = (r['data'] && r['data']['id'])
      ? relevamiento['data']['id']
      : Date.now().toString();

    this.geojson.addData(m);
  }

  constructor(public navCtrl: NavController,
    private relevamientos: RelevamientosProvider,
    private instancia: InstanciaProvider,
    private api: ApiProvider,
    public actionSheet: ActionSheetController,
    public popoverCtrl: PopoverController,
    private storage: NativeStorage,
    private geolocation: Geolocation,
    private network: Network,
    private categorias: CategoriasProvider,
    private toast: ToastController) {

    L.ColorIcon = L.DivIcon.extend({
      options: {
        html: '<div class="pin-background"></div><ion-icon name="pin" class="icon icon-md ion-md-pin"></ion-icon>',
        className: 'marker'
      },

      createIcon: function(oldIcon) {
        var icon = L.DivIcon.prototype.createIcon.call(this, oldIcon);
        icon.style.color = this.options.color;

        return icon;
      }
    });

    L.colorIcon = function(options) {
      return new L.ColorIcon(options);
    }

    L.GeoIcon = L.DivIcon.extend({
      options: {
        html: '<div class="pin-background"></div>'
          +'<ion-icon name="pin" class="icon icon-md ion-md-pin"></ion-icon>'
          +'<div class="pin-foreground"><ion-icon name="locate" class="icon icon-md ion-md-locate"></ion-icon></div>',
        className: 'gps-marker'
      },

      createIcon: function(oldIcon) {
        var icon = L.DivIcon.prototype.createIcon.call(this, oldIcon);
        icon.style.color = this.options.color;

        return icon;
      }
    });

    L.geoIcon = function(options) {
      return new L.GeoIcon(options);
    }

    // Monitorear el estado de la conexión para saber si tenemos que
    // enviar los relevamientos ahora o más tarde

    this.network.onDisconnect().subscribe(() => this.api.desconectar());

    this.network.onConnect().subscribe(() => {
      this.api.conectar();

      if (this.api.browser) return;

      this.instancia.authenticate(null, null,
        () => {
          this.sincronizar_categorias();
          this.sincronizar_relevamientos();
        });
    });
  }

  // Obtener todos los relevamientos offline y enviarlos a la API
  sincronizar_relevamientos () {
    this.storage.keys().then(keys => {
      keys.forEach(key => {
        if (!key.startsWith('relevamiento_offline_')) return;

        this.storage.getItem(key).then(relevamiento => {
          this.relevamientos.save(relevamiento).subscribe(res => {
            if (res.status !== 200 && res.status !== 201) return;

            // Si lo enviamos correctamente, eliminar de la cola
            this.storage.remove(key).then(() => true);
          });
        });
      });
    });
  }

  // Obtener las categorias y guardarlas en cache
  sincronizar_categorias () {
    this.categorias.getAll(true).subscribe((data: { data: any[] }) => {
      this.storage.setItem('cache_categorias', data);
    });

    const categorias = Object.keys(this.categorias_guardadas)
      .filter(k => this.categorias_guardadas[k]);

    // Traer y cachear todas las categorias que estamos usando
    categorias.forEach(categoria => {
      this.categorias.find(parseInt(categoria)).subscribe(data => {
        this.storage.setItem(`categoria_${categoria}`, data);
      });
    });
  }

  ionViewDidLoad() {
    this.center = [-34.6107,-58.4246];
    this.zoom = 12;

    this.drawMap();

    this.createEvents();
    this.storage.getItem('categorias_guardadas').then(
      data => {
        this.categorias_guardadas = data;

        if (this.api.browser) {
          this.categorias_guardadas = Object.assign(this.categorias_guardadas, this.getCategoriasFromParams());
        }

        this.getAllRelevamientos(this.categorias_guardadas);
      },
      error => {
        if (this.api.browser) {
          this.categorias_guardadas = this.getCategoriasFromParams();
          this.getAllRelevamientos(this.categorias_guardadas);
        } else {
          this.showToast('Seleccioná las categorías que quieras ver en el mapa');
        }
      }
    );

    /* XXX: En los celulares el evento de conexión no sucede si el
     * celular ya está conectado */
    if (!this.api.browser) {
      this.instancia.authenticate(null, null,
        () => {
          this.sincronizar_categorias();
          this.sincronizar_relevamientos();
        });
    }
  }

  getCategoriasFromParams() {
    var params = new URLSearchParams(window.location.search.split('?', 2)[1]);
    var categorias = {};
    params.get('categorias').split(',').forEach(x => categorias[x] = parseInt(x));

    return categorias;
  }

  getParamsFromCategorias(data: object) {
    return 'categorias='+Object.keys(data).join(',');
  }

  showTempToast(message: string) {
    const toast = this.toast.create({
      message: message,
      duration: 3000
    });

    toast.present();
  }

  showToast(message: string) {
    const toast = this.toast.create({
      message: message,
      showCloseButton: true,
      closeButtonText: 'Ok'
    });

    toast.present();
  }

  showActionSheet(marker, latlng) {
    const action_sheet = this.actionSheet.create({
      title: 'Relevamiento',
      buttons: [
        {
          text: 'Agregar',
          icon: 'add',
          handler: () => {
            if (this.api.browser && !this.login) {
              this.showTempToast('Tenés que iniciar sesión para poder cargar relevamientos');
              if (marker) this.map.removeLayer(marker);
            } else {
              this.addRelevamiento(latlng);
            }
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          icon: 'close',
          handler: () => {
            if (marker) this.map.removeLayer(marker);
          }
        }
      ]
    });

    action_sheet.present();
  }

  getAllRelevamientos(categorias: Object) {
    let filtro = new Array;
    if (categorias != null) {
      filtro = Object.keys(categorias).filter(key => categorias[key]);
    }

    this.sincronizar_categorias();

    // Cachear los relevamientos al traerlos.  Si no los podemos traer,
    // mostramos los de la caché
    this.relevamientos.getAll(filtro).subscribe(data => {
        this.geojson.clearLayers();
        this.geojson.addData(data);
        this.storage.setItem('cache_relevamientos', data);
      }, error => {
        this.storage.getItem('cache_relevamientos').then(data => {
          this.geojson.clearLayers();
          this.geojson.addData(data);
        });
      });
  }

  presentPopover(e) {
    let popover = this.popoverCtrl.create(CategoriasPage, {
        categorias_guardadas: this.categorias_guardadas
      },
      { cssClass: 'categorias-popover' });

    popover.onDidDismiss(data => {
      if (data != null) {
        this.getAllRelevamientos(data);
        if (this.api.browser) {
          history.pushState({}, "", window.location.pathname + '?' + this.getParamsFromCategorias(data));
        }
      }
    });

    popover.present({
      ev: e
    });
  }

  authenticatePopover(e) {
    let popover = this.popoverCtrl.create(LoginPage);

    popover.onDidDismiss(data => {
      if (data && data['identifier'] && data['password'])
        this.instancia.authenticate(data['identifier'], data['password'],
        () => {
          this.login = true;
          this.showTempToast('Iniciaste sesión, ahora podés cargar relevamientos.  Podés cerrar la sesión usando el mismo botón.');
          this.sincronizar_categorias();
          this.sincronizar_relevamientos();
        });
    });

    popover.present({ ev: e });
  }

  cerrarSesion(e) {
    this.login = false;
    this.storage.remove('token').then(() => this.showTempToast('Cerraste sesión.'));
  }

  loginVisible() : boolean {
    return (this.api.browser && !this.login);
  }

  addRelevamiento(latlng) {
    this.navCtrl.push(RelevamientoPage, {
      coordenadas: latlng,
      callback: this.crearRelevamientoAlVolver.bind(this)
    });
  }

  getIcon(color: string) {
    return L.colorIcon({color: color});
  }

  pointToLayer(feature, latlng) {
    return L.marker(latlng, { icon: this.getIcon(feature.properties.color) });
  }

  onEachFeature(feature, layer) {
    layer.on('click', (e) => {
      this.navCtrl.push(FichaPage, {
        relevamiento_id: e.sourceTarget.feature.id,
        login: this.login
      });
    });
  }

  createEvents() {
    this.map.on('zoomend', (event) => {
      if (this.map.getZoom() >= 15 && !this.sin_notificacion_en_zoom) {
        this.showTempToast('A partir de este zoom podés agregar puntos');
      }

      this.sin_notificacion_en_zoom = false;
    });

    ['click', 'contextmenu'].forEach(name => {
      this.map.on(name, (event) => {
        if (this.map.getZoom() < 15) {
          this.showTempToast('Podés agregar puntos al llegar a un zoom más cercano.');
          return;
        }

        this.tmp_marker = new L.marker(event.latlng, { icon: this.getIcon('#5892C8') }).addTo(this.map);
        this.showActionSheet(this.tmp_marker, event.latlng);
      });
    });
  }

  ionViewDidEnter() {
    this.map.invalidateSize();

    // Eliminar el marcador temporal si estamos volviendo de
    // relevamientos
    if (this.tmp_marker) {
      this.map.removeLayer(this.tmp_marker);
    }
  }

  drawMap() {
    this.map = new L.map('mapa', {
      center: this.center,
      zoom: this.zoom
    });

    L.tileLayer(this.tiles, {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
    }).addTo(this.map);

    L.tileLayer(this.parcelario, {
      attribution: '',
      minZoom: 15,
      maxZoom: 18,
      id: 'mapbox.streets',
    }).addTo(this.map);

    this.geojson = new L.GeoJSON(undefined, {
      pointToLayer: ((f,l) => this.pointToLayer(f,l)),
      onEachFeature: ((f,l) => this.onEachFeature(f,l))
    }).addTo(this.map);
  }

  geoLocate() {
    this.geolocation.getCurrentPosition(this.opciones_de_geolocalizacion).then(resp => {
      var coords = [ resp.coords.latitude, resp.coords.longitude ];
      var obj_coords = { lat: coords[0], lng: coords[1] };
      this.center = coords;
      this.zoom = 16;

      this.sin_notificacion_en_zoom = true;
      this.map.setView(this.center, this.zoom);

      if (this.tmp_marker) this.map.removeLayer(this.tmp_marker);
      this.tmp_marker = new L.marker(coords, { icon: L.geoIcon({color: 'red'}) }).addTo(this.map);
      // No eliminar el marcador al cancelar
      this.showActionSheet(undefined, obj_coords);
    }).catch(error => {
      this.showToast('No pudimos encontrar tu ubicación.');
    });
  }
}
