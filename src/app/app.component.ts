import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MapaPage } from '../pages/mapa/mapa';
import { AcercaPage } from '../pages/acerca/acerca';
import { NuevasCategoriasPage } from '../pages/nuevas-categorias/nuevas-categorias';
import { QuienesSomosPage } from '../pages/quienes-somos/quienes-somos';
import { QueRelevamosPage } from '../pages/que-relevamos/que-relevamos';
import { AyudaPage } from '../pages/ayuda/ayuda';
import { ContactoPage } from '../pages/contacto/contacto';
import { OfflinePage } from '../pages/offline/offline';

import { ApiProvider } from '../providers/api/api';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = MapaPage;

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private api: ApiProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Pendientes', component: OfflinePage, icon: 'paper-plane' },
      { title: 'Acerca', component: AcercaPage, icon: 'information-circle' },
      { title: 'Qué relevamos', component: QueRelevamosPage, icon: 'map' },
      { title: 'Quiénes somos', component: QuienesSomosPage, icon: 'people' },
      { title: 'Nuevas categorías', component: NuevasCategoriasPage, icon: 'pin' },
      { title: 'Ayuda', component: AyudaPage, icon: 'help-circle' },
      { title: 'Contacto', component: ContactoPage, icon: 'at' }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.push(page.component);
  }
}
