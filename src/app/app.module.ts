import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { ApiProvider } from '../providers/api/api';
import { InstanciaProvider } from '../providers/instancia/instancia';
import { RelevamientosProvider } from '../providers/relevamientos/relevamientos';
import { CategoriasProvider } from '../providers/categorias/categorias';
import { ComentariosProvider } from '../providers/comentarios/comentarios';
import { SubscripcionesProvider } from '../providers/subscripciones/subscripciones';
import { NominatimProvider } from '../providers/nominatim/nominatim';

import { MyApp } from './app.component';
import { MapaPage } from '../pages/mapa/mapa';
import { FichaPage } from '../pages/ficha/ficha';
import { AcercaPage } from '../pages/acerca/acerca';
import { NuevasCategoriasPage } from '../pages/nuevas-categorias/nuevas-categorias';
import { QuienesSomosPage } from '../pages/quienes-somos/quienes-somos';
import { QueRelevamosPage } from '../pages/que-relevamos/que-relevamos';
import { AyudaPage } from '../pages/ayuda/ayuda';
import { ContactoPage } from '../pages/contacto/contacto';
import { CategoriasPage } from '../pages/categorias/categorias';
import { RelevamientoPage } from '../pages/relevamiento/relevamiento';
import { LoginPage } from '../pages/login/login';
import { OfflinePage } from '../pages/offline/offline';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    MyApp,
    MapaPage,
    FichaPage,
    AcercaPage,
    QueRelevamosPage,
    NuevasCategoriasPage,
    QuienesSomosPage,
    AyudaPage,
    CategoriasPage,
    RelevamientoPage,
    ContactoPage,
    LoginPage,
    OfflinePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapaPage,
    FichaPage,
    AcercaPage,
    QueRelevamosPage,
    NuevasCategoriasPage,
    QuienesSomosPage,
    AyudaPage,
    CategoriasPage,
    RelevamientoPage,
    ContactoPage,
    LoginPage,
    OfflinePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    InstanciaProvider,
    RelevamientosProvider,
    CategoriasProvider,
    ComentariosProvider,
    SubscripcionesProvider,
    NominatimProvider,
    NativeStorage,
    Geolocation,
    Camera,
    File,
    Network
  ]
})
export class AppModule {}
