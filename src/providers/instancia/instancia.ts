import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { ApiProvider } from '../api/api';

@Injectable()
export class InstanciaProvider {
  public  token: string | null;
  private endpoint: string = '/instancias';
  private identifier: string | null;
  private password: string;
  private api_url : string;

  constructor(private api: ApiProvider, public http: HttpClient, private storage: NativeStorage) {
    this.api_url = this.api.url + this.endpoint;
  }

  authenticate(identifier: string = null,
    password: string = null,
    callback: any = null) : void {
    // Si estamos desde el navegador, le vamos a estar mandando las
    // credenciales a mano
    if (this.api.browser) {
      this.identifier = identifier;
      this.password = password;
      if (this.identifier && this.password) this.login(callback);
    } else {
      // Almacenar las credenciales en un solo registro, de forma que si
      // uno falla, falla todo el proceso y hay que volverse a registrar.
      this.storage.getItem('credentials').then(
        credentials => {
          this.identifier = credentials.split(':').shift();
          this.password = credentials.split(':').pop();
          if (this.identifier && this.password) {
            this.login(callback);
          } else {
            this.register(callback);
          }
        },
        error => this.register(callback));
    }
  }

  save_credentials() {
    var credentials = this.identifier + ":" + this.password;
    this.storage.setItem('credentials', credentials).then(
      () => console.log('Credenciales guardadas'),
      error => console.log('Error guardando credenciales: '+error)
    );
  }

  // La contraseña es aleatoria
  private generatePassword() : string {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  }

  // Obtiene un identificador
  register(callback: any = null) {
    this.password = this.generatePassword();
    // Si no existe, usar la contraseña para obtener uno nuevo
    var data = { instancia: { password: this.password }};
    this.http.post(this.api_url, data, { observe: 'response' })
      .subscribe(res => {
        if (res.status == 201) {
          this.identifier = res.body['data']['attributes']['identifier'];
          this.login(callback);
          this.save_credentials();
        }
      });
  }

  // Obtiene un token de autenticación
  login(callback: any = null) {
    var data = {
      instancia: {
        identifier: this.identifier,
        password: this.password
      }
    };

    this.http.post(this.api_url+'/login', data, { observe: 'response' })
      .subscribe(
        res => {
          if (res.status == 200) {
            if (callback) callback();

            this.token = res.body['access_token'];
            this.storage.setItem('token', this.token).then(
              () => console.log('Token guardado'),
              error => console.log('Error guardando token: '+error)
            );
          }
        }
      );
  }
}
