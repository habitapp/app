import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { InstanciaProvider } from '../instancia/instancia';

@Injectable()
export class SubscripcionesProvider {
  private endpoint: string = '/subscripciones';
  private api_url: string;

  constructor(private api: ApiProvider,
    public http: HttpClient,
    private instancia: InstanciaProvider) {
    this.api_url = this.api.url + this.endpoint;
  }

  create(data: Object) {
    return this.http.post(this.api_url, data, {
      observe: 'response',
      headers: this.jsonApiHeaders()
    })
  }

  jsonApiHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/vnd.api+json',
      'Authorization': 'Bearer '+this.instancia.token
    });
  }
}
