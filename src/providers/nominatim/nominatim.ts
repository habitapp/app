import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class NominatimProvider {
  private api : string = "https://nominatim.openstreetmap.org/reverse";

  constructor(public http: HttpClient) { }

  get(lat : number, lon : number) {
		return this.http.get<Nominatim>(this.api, { params: this.params(lat,lon) });
  }

  params(lat : number, lon : number) : HttpParams {
    return new HttpParams()
      .set('format', 'json')
      .set('lat', lat.toString())
      .set('lon', lon.toString())
      .set('addressdetails', '1')
      .set('accept-language', 'es')
      .set('email', 'nominatim@habitapp.org');
  }
}

export interface Nominatim {
	place_id: string,
  licence: string,
  osm_type: string,
  osm_id: string,
  lat: string,
  lon: string,
  display_name: string,
  boundingbox: string[],
  address: {
    house_number: string,
    road: string,
    suburb: string,
    city: string,
    state: string,
    postcode: string,
    country: string,
    country_code: string
  },
}
