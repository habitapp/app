import { Injectable } from '@angular/core';

@Injectable()
export class ApiProvider {
  public url: string = 'https://habitapp.org/api';
  public browser: boolean = false;
  public control_popular_de_precios: number = 198;
  public conectado: boolean = false;

  desconectar() : void {
    this.conectado = false;
  }

  conectar() : void {
    this.conectado = true;
  }
}
