import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { InstanciaProvider } from '../../providers/instancia/instancia';

@Injectable()
export class ComentariosProvider {
  endpoint : string = '/relevamientos/:relevamiento/comentarios';

  constructor(private http: HttpClient,
    private api: ApiProvider,
    private instancia: InstanciaProvider) {
  }

  api_url(relevamiento : number) {
    return this.api.url + this.endpoint.replace(':relevamiento', relevamiento.toString());
  }

  getAll(relevamiento : number) {
    return this.http.get(this.api_url(relevamiento), { observe: 'response'});
  }

  create(relevamiento : number, data : Object) {
    return this.http.post(this.api_url(relevamiento), data, {
      observe: 'response',
      headers: this.jsonApiHeaders()
    });
  }

  jsonApiHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/vnd.api+json',
      'Authorization': 'Bearer '+this.instancia.token
    });
  }
}
