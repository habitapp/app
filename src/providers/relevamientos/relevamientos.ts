import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { InstanciaProvider } from '../../providers/instancia/instancia';
import { ComentariosProvider } from '../../providers/comentarios/comentarios';

@Injectable()
export class RelevamientosProvider {
  json_api: string = '/relevamientos{}.json';
  geojson_api: string;

  constructor(private api: ApiProvider,
    public http: HttpClient,
    private instancia: InstanciaProvider,
    public comentarios: ComentariosProvider) {
    this.geojson_api = this.api.url + '/relevamientos.geojson';
  }

  jsonapi_url(el: string = '') {
    return this.api.url + this.json_api.replace(/{}/, el);
  }

  getAll(categorias: Array<number> = []) {
    return this.http.get(this.geojson_api, { params: { categoria: categorias.join(',') } });
  }

  find(id: number) {
    return this.http.get(this.jsonapi_url('/' + id.toString()),
      { observe: 'response' });
  }

  save(data: Object) {
    if (data['data']['id']) {
      return this.http.put(this.jsonapi_url('/'+data['data']['id']),
        JSON.stringify(data),
        { headers: this.jsonApiHeaders(), observe: 'response' });
    } else {
      return this.http.post(this.jsonapi_url(),
        JSON.stringify(data),
        { headers: this.jsonApiHeaders(), observe: 'response' });
    }
  }

  jsonApiHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/vnd.api+json',
      'Authorization': 'Bearer '+this.instancia.token
    });
  }
}
