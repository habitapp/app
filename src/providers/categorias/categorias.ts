import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { InstanciaProvider } from '../../providers/instancia/instancia';

@Injectable()
export class CategoriasProvider {
  endpoint : string = '/categorias{}.json';

  constructor(public http: HttpClient,
    private instancia: InstanciaProvider,
    private api: ApiProvider) { }

  json_api(el : string = '') : string {
    return this.api.url + this.endpoint.replace(/{}/, el);
  }

  getAll(authenticated: boolean = false) {
    if (authenticated) {
      return this.http.get(this.json_api(), { headers: this.jsonApiHeaders() });
    } else {
      return this.http.get(this.json_api());
    }
  }

  find(id : number) {
    return this.http.get(this.json_api('/'+id.toString()));
  }

  jsonApiHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/vnd.api+json',
      'Authorization': 'Bearer '+this.instancia.token
    });
  }
}
