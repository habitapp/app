version      := $(shell cat package.json | jq .version)
sdk_version  := 26.0.3
unsigned_apk := platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk
signed_apk   := platforms/android/app/build/outputs/apk/release/app-release.apk
zipalign     := /opt/android-sdk/build-tools/$(sdk_version)/zipalign
logo         := assets/imgs/logo.png

android-release: for-android version production build sign
browser-release: for-browser version production build-browser sync

sync:
	rsync -av platforms/browser/www/ habitapp.org:/srv/http/habitapp.org/public/

config.xml: always
	sed -re 's/(version=)"[^"]+"/\1$(version)/g' -i config.xml

version: config.xml

development:
	sed -re "s,https://habitapp.org,http://localhost:3000,g" \
		   -i src/providers/api/api.ts

production:
	sed -re "s,http://localhost:3000,https://habitapp.org,g" \
		   -i src/providers/api/api.ts

for-browser:
	sed -re "s/(browser.*=).*/\1 true;/" \
		   -i src/providers/api/api.ts
	sed -re 's,(<preference name="SplashScreen" value=").*(" />),\1$(logo)\2,' \
	     -i config.xml

for-android:
	sed -re "s/(browser.*=).*/\1 false;/" \
		   -i src/providers/api/api.ts
	sed -re 's,(<preference name="SplashScreen" value=").*(" />),\1screen\2,' \
	     -i config.xml

build: version
	ionic cordova build --release --prod --aot android

build-browser:
	ionic cordova build --release --prod --aot browser

$(signed_apk): $(unsigned_apk)
	jarsigner -verbose -sigalg SHA1withRSA \
	  -digestalg SHA1 -keystore habitapp.keystore \
		$< habitapp
	rm -f $@
	$(zipalign) -v 4 $< $@

sign: $(signed_apk)

apk:
	@echo $(signed_apk)

install:
	adb install -r $(signed_apk)

reverse:
	adb reverse tcp:3000 tcp:3000

log:
	adb logcat -b all -c
	adb logcat

debug: reverse log

run:
	adb shell am start -n org.habitapp/.MainActivity

browser:
	ionic cordova run browser --nobrowser

serve:
	ionic serve -c --no-open

.PHONY: always
always:
